# root\_spark 

The project is an early approach to reading ROOT files as Spark DataFrames. Follows ideas from [root_pandas](https://github.com/ibab/root_pandas) and uses `root_numpy` internally.

## Example usage

The following code will map `test.root` file into a `DataFrame` with 4 partitions
```
from root_spark import root2spark
df = root2spark(sqlContext, 'test.root', 4)
```

Now you can use all standard Spark functions
```
>>> df
DataFrame[px: double, py: double, pz: double, ev: bigint]
>>> df.take(2)
[Row(px=0.019409453496336937, py=0.011825480498373508, pz=0.0005165688926354051, ev=0), Row(px=0.32718953490257263, py=0.037878211587667465, pz=0.10848774760961533, ev=1)]
```

If for any reason you would like to access the function from an SQLContext object below a bit dirty way to do so
```
>>> pyspark.sql.readwriter.DataFrameReader.root = lambda dfr, paths, nslices, key=None, columns=None, ignore=None, where=None, *args, **kwargs: root2spark(dfr._sqlContext, paths, nslices, key, columns, ignore, where, *args, **kwargs)
```

Then you can simply
```
>>> df = sqlContext.read.root('test.root', 4)
```
