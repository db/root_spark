
"""
A module that extends Spark to support the ROOT data format.
"""

from root_numpy import root2array, list_trees, list_branches
from root_numpy.extern.six import string_types
from fnmatch import fnmatch
import itertools
import re
import ROOT


__all__ = [
    'root2spark',
]

NOEXPAND_PREFIX = 'noexpand:'


def expand_braces(orig):
    r = r'.*(\{.+?[^\\]\})'
    p = re.compile(r)

    s = orig[:]
    res = list()

    m = p.search(s)
    if m is not None:
        sub = m.group(1)
        open_brace = s.find(sub)
        close_brace = open_brace + len(sub) - 1
        if sub.find(',') != -1:
            for pat in sub.strip('{}').split(','):
                res.extend(expand_braces(s[:open_brace] + pat + s[close_brace+1:]))

        else:
            res.extend(expand_braces(s[:open_brace] + sub.replace('}', '\\}') + s[close_brace+1:]))

    else:
        res.append(s.replace('\\}', '}'))

    return list(set(res))

def get_matching_variables(branches, patterns, fail=True):
    selected = []

    for p in patterns:
        found = False
        for b in branches:
            if fnmatch(b, p):
                found = True
            if fnmatch(b, p) and b not in selected:
                selected.append(b)
        if not found and fail:
            raise ValueError("Pattern '{}' didn't match any branch".format(p))
    return selected


def filter_noexpand_columns(columns):
    """Return columns not containing and containing the noexpand prefix.

    Parameters
    ----------
    columns: sequence of str
      A sequence of strings to be split

    Returns
    -------
      Two lists, the first containing strings without the noexpand prefix, the
      second containing those that do with the prefix filtered out.
    """
    prefix_len = len(NOEXPAND_PREFIX)
    noexpand = [c[prefix_len:] for c in columns if c.startswith(NOEXPAND_PREFIX)]
    other = [c for c in columns if not c.startswith(NOEXPAND_PREFIX)]
    return other, noexpand

def read_root_mod(paths, nslices, sliceid, key=None, columns=None, ignore=None, where=None, sampleOnly=False, *args, **kwargs):
    if not isinstance(paths, list):
        paths = [paths]
    # Use a single file to search for trees and branches
    seed_path = paths[0]

    if not key:
        trees = list_trees(seed_path)
        if len(trees) == 1:
            key = trees[0]
        elif len(trees) == 0:
            raise ValueError('No trees found in {}'.format(seed_path))
        else:
            raise ValueError('More than one tree found in {}: {}'.format(seed_path, trees))

    branches = list_branches(seed_path, key)

    if not columns:
        all_vars = branches
    else:
        if isinstance(columns, string_types):
            columns = [columns]
        # __index__* is always loaded if it exists
        # XXX Figure out what should happen with multi-dimensional indices
        index_branches = list(filter(lambda x: x.startswith('__index__'), branches))
        if index_branches:
            columns = columns[:]
            columns.append(index_branches[0])
        columns, noexpand = filter_noexpand_columns(columns)
        columns = list(itertools.chain.from_iterable(list(map(expand_braces, columns))))
        all_vars = get_matching_variables(branches, columns) + noexpand

    if ignore:
        if isinstance(ignore, string_types):
            ignore = [ignore]
        ignored = get_matching_variables(branches, ignore, fail=False)
        ignored = list(itertools.chain.from_iterable(list(map(expand_braces, ignored))))
        if any(map(lambda x: x.startswith('__index__'), ignored)):
            raise ValueError('__index__* branch is being ignored!')
        for var in ignored:
            all_vars.remove(var)

    tchain = ROOT.TChain(key)
    for path in paths:
        tchain.Add(path)
    nentries = tchain.GetEntries()

    if sampleOnly:
        start = 0
        end = min(nentries, 100)
    else:
        partsize = nentries / nslices
        # XXX could explicitly clean up the opened TFiles with TChain::Reset

        start = partsize * sliceid
        end = start + partsize if sliceid != nslices - 1 else nentries

    arr = root2array(paths, key, all_vars, start=start, stop=end, selection=where, *args, **kwargs)
    return arr

def root2spark(sqlContext, paths, nslices, key=None, columns=None, ignore=None, where=None, *args, **kwargs):
    """
    Read a ROOT file, or list of ROOT files, into a Spark DataFrame.
    Further *args and *kwargs are passed to root_numpy's root2array.
    If the root file contains a branch matching __index__*, it will become the DataFrame's index.

    Parameters
    ----------
    sqlContext: SQLContext
        SQLContext of the Spark session
    paths: string or list
        The path(s) to the root file(s)
    nslices: int
        Number of slices into which the file(s) should be parallelized
    key: string
        The key of the tree to load.
    columns: str or sequence of str
        A sequence of shell-patterns (can contain *, ?, [] or {}). Matching columns are read.
        The columns beginning with `noexpand:` are not interpreted as shell-patterns,
        allowing formula columns such as `noexpand:2*x`. The column in the returned DataFrame
        will not have the `noexpand:` prefix.
    ignore: str or sequence of str
        A sequence of shell-patterns (can contain *, ?, [] or {}). All matching columns are ignored (overriding the columns argument).
    where: str
        Only rows that match the expression will be read.

    Returns
    -------
        DataFrame created from matching data in the specified TTree

    """
#    Notes
#    -----
#
#        >>> df = read_root('test.root', 'MyTree', columns=['A{B,C}*', 'D'], where='ABB > 100')
#
#    """
    names = read_root_mod(paths, nslices, 0, key=key, columns=columns, ignore=ignore, where=where, sampleOnly=True, *args, **kwargs).dtype.names

    base = sqlContext._sc.parallelize(range(nslices), nslices)
    data = base.map(lambda sliceid: read_root_mod(paths, nslices, sliceid, key=key, columns=columns, ignore=ignore, where=where, sampleOnly=False, *args, **kwargs).tolist()).flatMap(lambda x: x)

    return sqlContext.createDataFrame(data, names, None)
